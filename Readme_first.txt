Regarding these .dll files....


I have provided these .dll files, which are from version 1.09b of D2:LoD, only because 
the 1.09c and 1.09d patches from Blizzard messed up the game.

In particular, any skills on items that are "chance-cast" (such as 10% chance to cast 
Fireball on striking, 8% chance to cast Frost Nova when hit, etc. etc. etc.) don't work 
in the 1.09 patches that came out after 1.09b.

Huge problem there, especially with vast numbers of items in my Sanctuary in Chaos mod.

If you have upgraded to a patch higher than 1.09b, you can use these .dll files to revert 
back while playing my mod or any other one in which you want 1.09b functionality.

You should ONLY need to replace the d2game.dll file in your D2 folder. I have been told 
by some that you should replace d2common.dll and d2client.dll...but that hasn't been my 
experience.

You are highly unlikely to need to use any of the other .dll files but I include them just 
in case you want to fully revert back to 1.09b.

CAUTION!!!!!! Remember, back up the original .dll files you plan to replace so that you 
can put them back in your D2 folder later...should you need to go back to running 1.09c or 
1.09d or whatever you had. Always back up files before swapping them out for new ones.

-Jeff Bouley

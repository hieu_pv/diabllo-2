Readme for the Middle Earth Mod

Vi Tinh 57
57 Tran Quang Khai Q.1 TP.HCM

The Changes (starting with the first version of the D2 ME mod):
We altered so many things that we can't even remember them all :) But we'll tell you now of the major 
ones that you should know of in order to build your character and know what to expect.

General changes:
Monster attackrate was lowered in order to make melee fighting without a shield a survivable option. 
Furthermore blocking of shields was drastically lowered, too. You should now be able to defend yourself 
while relying on high defense rating only. In order to balance this, all defense rate boosting skills were 
greatly reduced. Shields are still useful since they add more DR than in classic D2 and a blocking rate of 
25% still means you get hit 25% less.

Monsters generally have more hit points but don't deal much more damage.

Greater healing/mana potions grant double the amount of health/mana than stated on screen
Grand healing/mana potions grant four times as much health/mana than stated on screen.
This also means your health ball will fill much faster, allowing you to stay in combat a tick longer.

Stash, Cube and Inventory size was maximized by using Fusman's enhanced stashkit. Thanks to this you 
can now stay in the field much longer and collect all the sets you ever wanted

Town gossip and quest talk was changed to fit a somewhat strange Middle Earth story... (read for yourself 
:) Spoken text was removed, because we did not happen to have Sean Connery around to speak our text.

No XP penalty for death on nightmare or hell difficulty. If you want to be punished more severely for 
having your character killed, go get whipped with an angry cat.


Characters:

All characters have the same AR modifier (in the original game, there were different modifiers for the 
various classes, giving some classes an inherent bonus or malus).


Dunadan (Paladin)
-Zeal is limited to a certain amount of swings to make it a viable level 20 skill
-Blessed Hammer does now part magical, part physical damage in order to balance the hammerdin.
-The single resist auras provide much greater resistance than salvation.
-Might is now the aura that provides more damage output at the cost of lower range and no extra feature 
like concentration has.
-Fist of the heavens improved
-Holy Fire, Holy Shock and Sanctuary improved
-Holy Bolt improved
-Defiance maxes out at +100 DR (because of lower monster attackrate)

starts with: STR25, DEX20, ENR15, VIT25
gains 2 life per level and 2 mana
life per vit 3
mana per enr 1.5


Rohirrim (Amazon)
-Multishot maxes at 6 arrows and gets cheaper with rising level
-Critical strike reduced
-Exploding arrow improved
-Spear class weapons now deal less damage than normal two-handed weapons but are faster (this was 
needed to counter jab being overpowered)
-Impale now improves damage up to 500% and is uninterruptable
-Fend slightly faster
-Pierce reduced

starts with STR20, DEX30, ENR15, VIT20
gains 2 life per level and 2 mana
life per vit 3
mana per enr 1.5

Sorceress
-all lightning damage skills were drastically improved
-all fire damage skills were improved
-all cold damage skills were slightly improved
-Static Field reduced to 20%

starts with STR10, DEX25, ENR40, VIT10
gains 1 life per level and 3 mana
life per vit 1.5
mana per enr 3

Summoner (Necromancer)
-Masteries slightly improved
-Poison skills greatly improved
-Iron Maiden toned down a bit (damage returned)

starts with STR15, DEX25, ENR30, VIT15
gains 1.5 life per level and 2.5 mana
life per vit 2
mana per enr 2.5

Northman (Barbarian)
-Whirlwind is only usable with one-handed weapons (there was no other way - even with -200% damage 
(!!!) this skill was tested as being vastly overpowered. If you don't see this you probably never played a 
Spearazon or a Paladin). Mana cost is reduced, though.
-Masteries don't grant critical hit
-Ironskin reduced to 60%
-natural resist reduced to 30%
-warcry improved

starts with STR30, DEX20, ENR10, VIT25
gains 3 life per level and 1 mana
life per vit 3.5
mana per enr 1

Items:
All unique items were changed. Those of you who played our Middle Earth mod for Diablo 1 will recognize 
quite some of them.
All set items were changed.
Both uniques and set items are often high level (mostly exceptional) items so don't be surprised if you 
don't find many early on. Most of the high level uniques have multiple versions of one base item type (for 
example there are six unique rings) so you have to find one after the other and keep the old ones in order 
to find the next.

Many base items are high level (60+) and thus only findable (and usable) by high level chars. The same 
goes for prefixes and suffixes. It is really worth it to play hell difficulty and battle monsters beyond level 60 
now.

Due to unavoidable hardcoded stuff you might encounter bosses who drop only a potion or other non-
magical crap. For the same reasons chests and the like cannot drop magical items as well. This is also 
true for special chests :(

Gems are slightly improved

mana potions are buyable (somewhat restricted)

Weapons are now more balanced:
-Spear class weapons are fast but don't deal as much damage
-Polearms are very slow but deal the most damage
-Crossbows are very slow but deal high damage
-Lowend Bows deal slightly more damage
-Swords, Axes and blunt weapons deal about the same damage at about the same speed.
-Throwing weapons come in greater stacks, deal more damage and use dexterity AND strength to 
calculate damage

Shields grant more DR than helms

Due to unavoidable hardcoded stuff high level base items with sockets are still unfindable :( we tried it out, 
and the effect of changing the treasure classes to allow socketed exceptionals was having cracked items 
dropped all over the place even in higher difficulty settings. Not nice.

Drops in general are much more random and mostly depend on the level of the monster.


Monsters:
Nearly all monsters have been changed. In classic D2 the combat values of monsters were sometimes 
really really strange so that sometimes two monster types of exactly the same level were very different in 
respects of the threat they posed to the player.
We equalized this so that a monster of a given level is about as dangerous as all other monsters of that 
level, be it a huge Walking Tree or a little Orc. 

In classic D2 each monster has a 20% chance to drop anything per player, with a cap at 95%. That meant 
that if 5 or more players were in the game nearly every monster would drop something.
In the mod monsters have a 25% chance to drop something for the first player and +5% per additional 
player with a cap at 40%.


Gambling
Unlike in the original game, it is now no longer certain that you always get one unique or set item when 
gambling for all available items. It can be the case that you get only standard magical items. Well, since 
nobody EVER does gamble for all items (or do you ?), this in the overall view (hundreds of games) simply 
means that you have slightly lower chances of gambling a unique item. You cannot gamble for exceptional 
items, because otherwise it would have been possible to gamble the best items in the game. We want you 
to find them from monsters, while actually playing the game.


Changes in version 1.3 (for Diablo2 "classic"):

Items

Throwing weapons (mostly the low level ones) have their damage toned down slightly

All blunt weapons are one level slower to compensate for the "+50% damage vs undead" effect (there are 
a lot of undead monsters in our mod, so this is a really nice effect)

Grand Healing and Grand Mana potions are only buyable in Hell difficulty

Ring and amulet drop chance slightly increased

The piercing affix on some set and unique bows is fixed

Attack speed of a unique dwarven xbow fixed

Crushing blow will remain on bows, because it works in the xpack (according to testers), and we definitely 
will carry the mod over to the xpack, and the bows will then work there :)

Gems other than diamonds now provide more single resistances (up to ~45%)

Blade and Double-Axe damage improved


Skills

Static Field mana cost rises with level (this is to limit the range (if you do not learn to a high level) or to 
limit the amount you can cast (if you learn to a high level). In other words, a nerf. But you can decided for 
yourself in which way you want to be nerfed, range or mana cost :)

Sorceress cold spells damage reduced to classic D2's values (25% reduction to the last ME mod version). 
Cold skills were good the way they originally were, as we found out.

Lightning skills that deal "1-x" damage have their max damage greatly increased (Lightning maxes at ~750 
and Chain lightning at ~500). Since we cannot change the "1-x" to "y-x", we can at least raise the average 
damage the skill deals.

Lightning and chain lightning are uninterruptable

Lightning mastery slightly increased (sorry that was all that was possible :) )

Whirlwind now grants +60% damage at level 20 instead of 0%. Should satisfy the Northman players.

Warcry stunlength reduced again somewhat.

Monsters

Saruman and some other set bosses now do no longer have abilities that made them a major nuisance

The Witchking now has the correct eight minions he should have

Balrogs are immune to revive and conversion

Project "no more walk in the park":
Normal difficulty monsters have their attackrate increased by 50%, their defenserate by 25% and their 
damage by 25%
Nightmare difficulty monsters have their attackrate increased by 25%
Hell difficulty monsters have their damage increased by 25% and their XPs increased by 20%

Monster density at end of acts slightly increased to prevent characters from entering areas with high level 
monsters too early (e.g. level 30 chars fighting in NM/4)

Spike fiends firing range reduced

Several AIs of monsters improved (from the monsters point of view :) )

Guldur Demon types fixed (they use two attacks but had all values for the second one (double arm attack) 
set to 0 )

Increased attackrate of all archers (monsters), in addition to the AR improvement above.

Some "new" monsters (sorry no new Balrogs or Dragons :) working still on them )

Misc

Shops now sell more diverse and random items. Also removed some overpowered items from early acts 
like tower shields in act 2.

Ranged weapons and mostly lighter armor and weapons are sold by Ted Sandyman, Barliman, Stybba 
and Eowyn while the other shopkeepers sell the heavy duty stuff.

Resistance penalty in nightmare is now 25% and in hell 60% (just to prepare you for the expansion :) )

Cowlevel disabled - we are sorry but we could not prevent the cowking from dropping illegal (for his level) 
uberitems, however hard we tried. He seems to use some fixed drop routines. Gollum's leg is now not 
usable anymore which effectively prevents you from entering the cowlevel. No, you do not want to know 
what would be there now :)

Several string fixes

max volume of music raised (only a "nice to have" feature - you can still adjust the volume via the sliders 
in the menu, but now it is possible to have the music louder than before - set the slider to halfway to get 
ca. the old volume level)

Notes

The item drop rate in some area / difficulty combinations is lower than it should be. We have tracked down 
the reason for this but unfortunately a fix would be rather complicated. The reason is that the game seems 
to use a very limited number of retries when the randomly chosen item is too high level for the dropping 
monster. To prevent this, we would have to create dozens of new lists (called treasureclasses) and 
honestly, we are not very eager to do so since this problem occurs only in the first act of normal difficulty.


Changes in version 1.4 (Diablo2 classic):

Skills:

We found out that the players complaining about the Sorceress' cold skills doing too much damage were 
Uberplayers. The skills NEED to do that much damage, else the game gets far too tedious. All other 
classes have their damage improved (skills, items), so the Sorceress needs higher damage, too. If the 
game is too easy for you, play a variant. Too many players complained about the game being a bit too 
hard.

Sorceress' armor skills now do a max 150% DR bonus instead of 100% (as an exchange for not being 
able to wear the highest DR items and not having a decoy etc.)

Valkyrie now has less hit points (is now like a monster lvl 30/50/70 in slvl 1)

Concentration now gets 75% uninterruptable to make it more interesting

Items:

Rings and Amulets are now buyable from the usual staff/wand sellers. They are relatively expensive (and 
thus give lots of money when selling). To keep game balance (money), this means that you now can no 
longer find rings/amulets in normal difficulty act 1 (but you still get the Cain quest reward ring).

 The chance for finding exceptional items has been improved slightly

2-H axes and mauls have range +1, the best axes (Giant Axe/Moria Axe) have range +2.

Dunish Spear, Pike, Rohirric Lance and Elven Lance have their range increased by one.

Blocking of exceptional shield has been modified.

Monsters:

No monster has 100% immunity to anything, max is 90%

"Breeding frequency" in the Maggot Lair has been decreased a bit (due to our improved monsters, you 
could get into quite a mess down there)

Classes:

Summoner and Sorceress now have improved attack speed and hit recovery (they are still a bit worse 
than the "combat" classes, but IMHO the differences in combat should come from skills/attributes, not from 
inherent weaknesses

Dunandan has the same bow attack speed as the Northman

General:

Item drops have been redone completely. Generally speaking, there is a new treasure class every five 
levels (for monsters), which should cause the item drops to be more in line with where you play. This has 
the effect that at the beginning of an act, the drops are a bit worse than at the end (because the first 
monsters can not drop all the items possible in that act). But overall, drops should be more interesting and 
useful. Of course this also means that there still is a chance of monsters dropping crap, but that is 
intentional (you cannot only find great items ...)






Changes in 1.5 (still Diablo2 classic):

Needs Diablo II v1.06b

Monsters:

New Monster Guardian Balrog replaces Balrogs (GBs are about as tough as former Balrogs)
Gothmog is now boss of Guardian Balrogs rather than Balrogs. Beware the inferno of Balrogs.

Balrogs are more dangerous now and randomly seeded.

Skills:

Valkyrie re-improved a bit. It seems the game ignored some of our new values and thus the Valkyrie 
became rather weak. She now maxes out at ~750 HP and should be able to deal considerable amounts of 
damage.

Elemental Arrows and Throwing skills of the Amazon changed back to "always hit".

Items:

All kind of mana pots (lesser to grand) are now buyable at the shops. This was the only way to have 
greater/grand mana pots in the shops at higher difficulties.

All Crossbows are now one level faster but still one level slower than equal bows.

Damage of crossbows adjusted.

Attack speed of spear-class weapons increased.

General:

You can now check your version at the title screen.



Changes in 1.6 (Diablo2 classic):

Monsters:

Terror Brood/Morgul Spawn and Angband Brood tougher (raised HP, AR and number)

Guardian Balrogs no longer immune to revive and conversion

The toughest monster have an additional 5% drop bonus for items (Melkoric Vampires, Brood of 
Ungoliant, Fallen Maia, Nazgul and Balrog)

Skills:

No more prerequiste skills! (We do not consider it cheating if you use a trainer program like Jamella's to 
remove never used prerequisite skills on your old ME chars and use these skill points otherwise, but Clans 
and Guilds might). This means that you can buy any skill in one of your trees as soon as you have 
reached the necessary character level, without having to buy lower level skills first. The game still checks 
skill lvl / char lvl difference, though - you cannot save up 10 skill points and invest them into chain lightning 
as soon as you reach the necessary char lvl, you have to buy the points one by one each lvl.

Slow Missiles fixed (was getting worse with increasing slvl)

Pierce slightly reduced in effectiveness

Decoy now has double HP of caster

Elemental and poison skills of Rohirrim improved

Double Throw toned down (no AR bonus and increased cost - should still be overpowered :)

Tunderstorm castable in town / telekinesis disabled in town
Non-aggressive warcrys are enabled in town
(both to prepare yourself before leaving town)

Vengeance damage slightly reduced and AR bonus increased (25 and +5 per level)

Might and Concentration bonus lowered - attack skills of Dunadan adjusted according (lvl 20 zeal and lvl 
20 might still result in the same damage as before for example). This change is to make other auras more 
viable.

Multishot reduced to 5 arrows and cost increased - sorry but this skill should not be effectively usable at 
slvl 1, you should have to invest into it to make it powerful

Items:

Non-sword/non-bow two-handed weapons speed increased

Mauls and two-handed axes have range 2 (0 = no range)

Some unique item bugs fixed. Guess this will never end :-)

Repeating and Dwarven Xbows faster (now actually faster than bows)

Scepters, wands and staff damage improved.

gems slightly improved

Some high end unique items have their damage boosted to make them useful (a high end unique weapon 
is useless if it does not do enough damage, even if the other attributes are very good)

General:

Fast/er/est cast items toned down (halved)

+%DR (glorious, awesome etc) improved / DR of body armor increased slightly / blocking slightly reduced. 
This is to make non-shield melee fighting possible and avoid the "you are less powerful if you do not use a 
shield" situation.

Droprate increased: 35% for a single player / +5% per additional player / capped at 50% / droprate of 
exceptionals in Maiar and Valar difficulty increased.

Some affixes changed to make midlevel ones (30-60) more viable

resistance malus in Maiar/Valar difficulty returned to old value (-25/-50) - after all it is difficult enough to 
gain positive resistances in Valar difficulty



Changes in 1.8 (Diablo2 classic):


General:

Sorry, no more .d2m version

We ported many of the new features of Blizzard's 1.08 patch, but not everything.

NOT ported were:

SF nerf. SF minimizes monster HP to 10% / 20% in Maiar and Valar diff resp. (instead of 33%/50%)

50% physical damage resist for all hell diff monsters

higher monster levels in NM and hell

casting delays

walk/run speed nerf

the mod is also playable in 800x600 mode. For this, you have to provide a new registry key (dword entry 
"resolution" with the value "1" in HKEY_LOCAL_MACHINE, Software, Blizzard Entertainment, Diablo II). 
Simply double click the provided registry file (in the zip) which permanently sets the game to 800x600 
mode. Note that this is more or less a hack, so it COULD cause some problems (has not yet for us), and 
you need a pretty powerful machine to run that mode. To go back to 640x480, run regedit, click through to 
the key mentioned above and simply remove it.

hirelings are slightly boosted (vs CD2 1.08) and cheaper

Skills:

We did make use of some of the new skill features:

fanaticism slightly increases damage

concentrate and frenzy add damage

lightning mastery increases damage

75% damage for multishot (hardcoded)/ 6 arrows per shot again and reduced cost

many skills should now have more of an increase with higher skill levels than with low ones

zeal has 2-6 swings

holy shock adds lightning damage to attack (1-350 at slvl 20)

Masteries have a chance of critical hit again

some other Northman skills (like Natural Resist and Iron Skin) have been improved

Items:

we use the new itemdrop system, thus item drops are now even more various. You might find a studded 
leather from a monster in Valar/4 and the next monster of the same type drops an Elven Royal Mail. This 
also means more frequent ring and gem drops and more rare/set/uniques of act end bosses (theoretically 
:) )

4 new sets

Vendors in Maiar and Valar now buy items for more money (Maiar 35K, Valar 50K)

baseitems all have a level requirement of 75% of their level now

set and unique items found from now on also have level requirements

arrows/bolts now are only stackable up to 500 because any number higher than 511 would crash the 
game


Changes for Lord of Destruction (1.0)
This mod is not new from scratch but "just" the Middle Earth Mod for Diablo 2 ported to the expansion set. 

There will be no more new mod versions for classic Diablo 2

This mod includes some changes and new features which are listed here:

General:

There are 9 crafting recipes

Attack weak: Magic Weapon + Jewel + Man Rune
Attack normal(only from Maiar onwards): Magic Weapon + Jewel + 2 Kir Runes
Attack good(only Valar): Magic Weapon + Jewel + Kir Rune + Ereg Rune + Sereg Rune

Defense weak: Magic Armor, Helm or Shield + Jewel + Anna Rune
Defense normal(only from Maiar onwards): Magic Armor, Helm or Shield + Jewel + 2 Thol Runes
Defense good(only Valar): Magic Armor, Helm or Shield + Jewel + Thol Rune + Curu Rune + Tin Rune

Surprise weak: Amulet + Jewel + 5 Er Runes
Surprise normal(only from Maiar onwards): Amulet + Jewel + Ram Rune + Fea Rune
Surprise good(only Valar): Amulet + Jewel + Ram Rune + Fea Rune + Amarth Rune 

Resistance penalty is -35 in Maiar diff and -60 in Valar diff

There is no global physical resistance in Valar diff (only bosses can be spawned with physical immunity)

There are no Runewords besides that of Act5 quest.

Hirelings do 50% damage to bosses.

Items:

Elite Items can have inherent abilites. Armors have reduced damage% and weapons can have deadly 
strike based on character level.

Many exceptional uniques are also available in elite version.

Throwing weapons damage is modified by dexterity and strength with each 50% (melee weapons 100% 
strength)

In Maiar difficulty you can buy exceptional high end items (ask Blizzard why that is not available in hell 
diff...)

re-implemented formerly deleted weapons and armor base items (Gothic Plate, Breast plate, Bastard 
Sword, Morning Star etc.)

exceptionals may be gambled for. If an item is exceptional or elite is only seen after it is purchased. 
Whether an item can be exceptional or elite is determined by player level.

rare and unique items should drop more often

rings and amulets could not be removed from the gamble screen, thus we made them too expensive to 
gamble (you should not be able to gamble them at all)

the level of runes is the same as their level requirement (as in classic LoD)

Monsters:

Dragons are dangerous :)

Pain Worm type monsters have high physical resist.


Characters/skills:

general adjusting of Beorning and Dark Elf skills to the ME environment.

Beorning's elemental attacks improved

Summoner's poison attacks improved

Northman's whirlwind and berserk improved

Static field stops at 10% in maiar and 20% in valar diff

Beorning life per level 2, life per vit 2.5, mana per level 2, mana per energy 2

Dark Elf life per level 2, life per vit 3, mana per level 2, mana per energy 1.5



Changes in Version 1.2 (LoD):

Monster level transition between difficulties smoothened. Monsters in act1 maiar are now level 35-40 (in 
act 2 maiar they are 39-45) and in valar act 1 they are level 60-65 (in act 2 valar they are 64-70).

Monster resistances modified. This mainly affects lower act5 since many monsters there were forgotten 
and had 0 resists.

Runes Thoron, Amarth, Khelek and Thalion renamed to Thor, Amar, Khel and Thal. This might not be 
linguistically correct but since rune names are restricted to 5 letters this is probably the best way 
(introducing new names from scratch is irritating).

fixed some bugs with rune words and unique items.

Rune words:

Weapons:
1. Man Curu Gurth
2. Kir Ang Sul
3. Ereg Sereg Ur Thal

Armour:
1. Anna Tir Maeg
2. Thor Ang Sul
3. Fea Sereg Beleg

added 22 class-specific uniques

Melee/Cold attack of Sauron Reborn toned down

modified many skills (mainly improving underpowered low level skills) this includes:

Impale (no durability loss)
magic arrow
fire arrow
cold arrow

firebolt
icebolt
inferno
blaze
thunderstorm and charged bolt slightly toned down (a level 20+ charged bolt is still viable against even 
Valar Monsters!)
chilling armor damage
shiver armor damage

poison creeper
spirit of barbs
rabies
maul
firestorm
molten boulder

blade sentinel
shock web
fireblast
charged bolt sentinel
wake of fire
blade fury
lightning sentry
blade shield
cloak of shadows
psychic hammer
fists of fire

Many of the damage improvements only apply to the upper skill levels. A level 25 Firebolt might now be a 
valid option over fireball for example.



Changes in Version 1.3B (LoD):

Added support for german version (texts will show up in english but you no longer need an english 
D2lang.dll if you play with the german version of LoD)

Shopkeepers sell more different items (previously especially boots were rare since they are listed last in 
the list the game uses to decide what to offer and some shopkeepers ran out of place)

The three sorceress masteries are now level 1 (to give those sorcs who want to specialize in high level 
spells something to spend their skill points in at early levels)

The resists of all monsters are now more equally distributed. Previously the distribution was very much 
towards cold resist (about 1:1,3:2,5 Lightning:Fire:Cold). It is now nearly 1:1:1

Class-specific item drops are rarer, they were dropped far too often which was pretty annoying.

Slow Missiles starts with 50% (was 30%)

Strafe slightly faster and range increased

Mulishot shoots 7 arrows (was 6)

Rohirrim class specific items may have +Rohirrim skills

Drops of Bosses, Champions and Act-end bosses should be better now

Rare items drop slightly more often

Rarities of unique items fixed

Items with +single skills affixes cheaper (mainly class specifics like Assassin Claws)

Rabies manacost lowered, duration increased slightly.

Physical Damage of Blessed Hammer fixed

Wraith-type monsters damage reduced

Exceptional high-end items are no longer available at shops in nightmare diff (we tried to move that 
feature to hell diff but it doesn't seem to work there) 

Added some new affixes with multiple effects. These are only available on magic items (not rares)

Superunique bosses (those that are always set in the same area) now always drop 2 of either amulet, 
ring, jewel or charm. This is because for some reason they often drop very very low level crap (that's an 
unresolved bug from classic D2LoD)

Gothmog, Glaurung and some other superunique bosses that must be killed to continue through the acts 
are toned down so that they are no complete "stoppers"

Charged Bolt toned down (should still be a viable option but not multiple times more efficient than 
highlevel skills)

New cube recipe to create runes #10 to #20:  5 runes of one type =>  1 rune of the next type

New cube recipes to socket normal items and to make normal items from magic ones:

Magic item + 3 jewels = the same item in normal form (not available for jewels, amulets, rings and charms)

Normal item + perfect gem = 1 socket in that item
Normal item + 2 perfect gems = 2 sockets in that item
Normal item + 4 perfect gems = 3 sockets in that item
Normal item + 8 perfect gems = 4 sockets in that item
Normal item + 16 perfect gems = 5 sockets in that item
Normal item + 32 perfect gems = 6 sockets in that item
(normal means normal, no socketed or superior items are allowed)

See Cubes'n'Runes.rtf for more details



Changes in Version 1.4 (LoD):

Charged Strike toned down (fixed 10 bolts instead of up to 42)

Immolation Arrow less explosion damage and casting delay of 1 second

Whirlwind now works with two weapons but got damage bonus decreased to max 100% (was 200%)
also WW now always hits (like it does in CLoD). This had to be taken over from CLoD to counter a bug 
that caused you to become immobile.

Leap bug (hopefully) fixed

Bash damage increased

Concentration Damage increased

Lightning Strike damage increased

All Spells now rise in damage non-linear. That means for example a Fireball will increase in damage by 9 
in early skilllevels (1-8), in midlevel it will increase by 17 and from level 15 upwards it will rise by 24. That 
way even a highlevel magic user can improve his damage output by finding items with +skills

Amulets and Rings are now available for gamble again at a reasonable price (chances to get uniques or 
sets via gambling seem to be near zero)

+skill affixes for invalid charclasses on classspecific items should now no longer spawn

Staffs, scepters and wands drop slightly less often (these items are inbetween normal items and 
classspecific items - thus they are practically unusable by most chars)

Affixes with +AC per characterlevel slightly decreased
Affixes with +%AC per characterlevel slightly increased
(applies only to newly spawned items)

Number of Sockets changed for some items to make them have the same amount of sockets in normal, 
exceptional and elite form. Check the updated socket list in this readme. No item should have less sockets 
than before (if so report it)

Sauron Reborn's Fire blast attack severly toned down.

Mumakil-type stomp attack slightly toned down

Added five new monsterclasses, derived from Druid's summons and Ancient Barbarians graphics.

Regeneration rate of most monsters toned down

Resistances of Valar diff monsters toned down to those of Maiar diff (after all its already a percentage)

Some set items and many unique items improved. This only applies for newly spawned items

New crafting recipes (see cubesnrunes.txt)

Droprate of exceptional and elite items slightly toned down

Monster density in Angband slightly decreased to avoid stairtraps

Experience for Maiar and Valar difficulties reduced by 25% (getting level 75 and becoming 
Patriach/Matriarch without having to repeat anything at all might be nice but kills longevity)

Valar diff monsters +25% HP

Added three new sets and ten new uniques, mostly consisting of elite baseitems. 




Changes in Version 1.4B (LoD):
(this version is needed for Blizzard's Patch 1.09c)

Some super unique monsters like Bert (the smith) or the Council of Dol Guldur dropped way too good 
base items. This has been fixed

Charms are somewhat less powerful and more rare.

Super unique monster that formerly dropped two rings/amus/charms/jewels now only drop one of these 
items.

The quest reward for the fifth quest in act 5 got reduced somwhat:
Formerly you gained 1.4 mio XP in Eldar, 20 mio in Maiar and 40 mio in Valar. This got reduced to 1 mio, 
10 mio and 30 mio. (Gaining 5 levels with a level 47 character only by killing those three guys was 
definitely unbalanced) .

The Imp type monsters are spawned a bit less often. They still are found often, because they seem to be 
hardcoded to spawn with the siege structures of act 5 and with the Mumakil type monsters.




Changes in Version 1.4D (LoD):

Dropbugs hopefully fixed :)



Changes in Version 1.5 (LoD):
(this version is needed for Blizzard's Patch 1.09d)

Diablo 1 Blood Knight Monster models used for four new act 5 monsters (thx to Foxbat)

Magic item drops from normal monsters slightly more frequent

Baseitemdrop ratio "smoothened": high-end baseitems (70+) slightly rarer, mid- and lowlevel baseitems 
more frequent and transitions between the different Treasureclasses more smooth (formerly there were 
several sudden increases and decreases between those).
This should result in a decrease in very low level items from highlevel  monsters (such as clubs or leather 
armors from Valar diff Balrogs).
If you don't know what we are talking about, don't worry - it's nothing major.

Decreased the dropping of gems (especially chipped ones).

Maggot Lair renamed to Lair of the Deathless and Maggot boss replaced.

Bug with Tristram/Tharbad/Cairn Stones and the transition between Upper Dol Guldur and Dol Guldur 
fixed.

Non-magic ring amulet bug fixed. 

Added two new unique rings, amulets and circlets

Damage of fast one-handed weapons (long sword, flail, double axe etc.) and some claws slightly 
increased

Low level and overall crappy affixes no longer spawn from highlevel sources (in shops this would be your 
characterlevel, from monsters it would be their monsterlevel). The thresholds for this are most of the time 
30, 50 and 60. 




Changes in Version 1.6 (LoD):

All Monsters in act 5 Valar have their combat values increased by 25-50%

Resist malus in maiar and valar diff raised to 50 and 90 (to add challenge)

Stamina Drain vastly increased (running is meant to work now only for short distances or in town)

Walk and Run speed slightly lowered - some monsters' speed increased (to add challenge)

Telekinesis enabled in town

Elite versions of Bone Helm, Bone Shield and Spiked Shield can now have sockets

Balrogs and Dragons had some problems hitting or causing damage in Valar diff - this has been fixed

Blade Fury and most Dark Elf (Assassin) trap skills improved

Summoner (Necromancer) Revives receive no hitpoint bonus but have a longer duration

Frost Nova slightly improved

Beorning (Druid) Vines toned down

Some Beorning (Druid) shapshifting skills toned down

High-end unique items have higher level requirements

Corpse Explosion toned down

Amplify Damage toned down

Frozen Orb toned down

Imps and Enslaved now don't get spawned at certain locations in act 5 all the time.

Various string fixes

Holy Bolt heal and damage rate increased

Set Item bow from the Halfling's Set fixed (bow had +block%)

No more +blocking  on two-handed weapons (affix series "of fleetness" now grants +walkspeed instead of 
+blocking) 

Two new unique axes (Dramborleg, Tuor's Axe and Maeglin's Cleaver - both Tusk Axes) 

Creating the next higher Rune from three/five of one type changed:
Runes # 1-14 now require three to create one rune of the next higher type
Runes #15-24 now require five to create one rune of the next higher type

New Rune recipes:
Any unique item -> a rare item of the same type with quality 80% player level / 80% itemlevel

Any rare item + jewel + rune + charm + perfect gem + any set item + any crafted item
-> rare item of the same type with 1 socket and quality 60% player level / 60% itemlevel 

Any rare item + 2 jewels + 2 runes + 2 charms + 2 perfect gems + 2 set items + 2 crafted items
-> rare item of the same type with 2 sockets and quality 75% player level / 75% itemlevel

Any rare item + 3 jewels + 3 runes + 3 charms + 3 perfect gems + 3 set items + 3 crafted items
-> rare item of the same type with 3 sockets and quality 85% player level / 85% itemlevel

(these recipes are really only useful with highlevel items and most important high level chars (85% of level 
75 is 64 and level 64 affixes can be quite powerful)




Changes in Version 1.7 (LoD):

Staminadrain nerf nearly back to normal

Runspeeds back to normal

Monsters slightly faster (run you fools! :)

Crash bug in act 5 cursed cave fixed

Blessed Hammer physical damage removed and added to magic damage (which is enhancable by 
concentration). Physical damage didn't show up on characterscreen for some reason.

Titles for killing Morgoth now genderspecific again (thx Nath)

Five new Runewords for weaker Runes (see Cubes'n'Runes.rtf)

Wake of Inferno trap changed to a cold trap with the same damage and way of attack




Changes in Version 1.8 (LoD):

Due to a rather awkward formula in item generation mid to highlevel baseitems were prone to only feature 
lowlevel affixes (you find a Rohirric Lance and it only has "of the Glutani" on it while sabres and daggers 
constantly have "Kings" or "of Slaughter"). We bypassed that formula so that now all baseitems have the 
same chance for any affixes. This will effectivly improve the quality of itemdrops in nightmare drastically 
(hopefully :)

Hirelings should aquire levels easier now

Molten Boulder and Firestorm slightly enhanced

Enchant enhanced

Fanatiscm range inreased

Poison Nova damage increased

BoneWall/Prison HP increased

The cuberecipe that allowed more than one socket on a rare item is now fixed and works

Some Runewords improved (see Cubes'n'Runes.rtf)

Runspeeds of monsters decreased slightly again

Running for longer times is now possible if you don't wear heavy armor.Very heavy armor slows you down 
and drains much stamina. We have added a few affixes that increase stamina and stamina recovery rate. 
So if you want to run around all day it is possible but at a price.

Most class specific items now either have +skills or an effect (like resistance for Dunadan shields) built in 
and not both. This is to counter +bow skills on amazon spears and the likes.

Dragons now drop items like champions (no pun intended :)

Devourer monsters in act 4 no longer eat corpses

We added some 2 word runewords (see cubes'n'runes.rtf)

Some jewel and charm affixes have been improved (lower level mostly)

Normalizing magic gloves and boots recipes added (we forgot about them)

5 gems of any type but the same quality can now be cubed to one gem of any type but a higher quality

Spikefiend type monsters and archer skeletons attackrate improved (was not in accordance with other 
ranged fighters of like level)

+%AC effect of Diamonds slightly improved

Healing Potions improved. Were 50/75/120/200/350 and are now 50/80/150/275/480. These are 
basevalues that are modified by characterclass and the correct amount of gained health is also not shown 
on the screen if you hold the cursor on a potion. This change is a dll change that is only active if you use 
our D2game.dll

Throwing weapons use larger stacks and replenish slowly (built-in ability)

Poison Javelin, Plague Javelin and Lightning Fury cost less mana to use (to counter their inability to leech 
mana) and are increased in damage.

Lightning fury increased damage but fixed at 4 bolts (can split into more by piercing targets)

Lightning Bolt more damage

Two runes of the same type now create a runeword that adds some of the rune' effect as bonus (about 
50-75% of the rune's effect)

Three runes of the same type now create a runeword that adds most of the rune' effect as bonus (about 
75-100% of the rune's effect)
 
Throwing weapons can have sockets but for some reason the game will not allow dropped throwing 
weapons to have sockets. Therefore you will have to either use the cube recipes (perfect gems + normal 
weapon => socketed weapon) or a magic throwing weapon with the socketing affix (jeweler's etc.). 
Throwing weapons can have as many sockets as their graphics support which means a maximum of 4. 



Changes in Version 1.9 (LoD):
(this version is needed for Blizzard's Patch 1.10)


Many unique items quality enhanced, some of them greatly

Unique items and runes drop more often

Most Runewords and crafting recipes are now easier to create (see CubesnRunes.rtf)

Monsters in Maiar and Valar difficulty have a 20% chance to have one or more of the following:
Faster walk
Faster attack
Faster cast
Slightly increased elemental resistance
Slightly increased physical resistance
Slightly increased hitpoints 

Monsters in later difficulties make increasingly less pauses between attacks

Increased inventory space to the maximum and reduced the size of books to 1 slot (switching from version 
1.9 to 1.8 could cause problems!)

Maximum possible Sockets per item type changed (see CubesnRunes.rtf) . Normal items now can only 
hold 2 sockets for helms and three sockets for the rest while higher level exceptional and elite items 
normally can have more. The maximum possible sockets are now INDEPENDANT from difficulty or 
playerlevel.

Whirlwind has a casting delay of 1,5 seconds now (being constantly in whirlwind mode makes you nearly 
invincible since you leech life all the time and automatically evade most projectile attacks by being a 
moving target)

Casting Delay removed from Chain Lightning

Casting Delay removed from Blizzard and added to Frozen Orb

Set items now spawn with a socket (if that item can have a socket) and Full Set Bonus increased 
considerably

Elemental Damage Bow Skills (Rohirrim) casting cost reduced

Burst of speed duration increased

+damage vs Demons/Undead affix changed to suffix to make it usable alongside prefixes such as King's 
or Merciless

New affixes added that greatly improve damage vs certain monstertypes (indicated under the 
monstername): Humans, Animals, Orcs, Trolls and Greater Demons.
Greater Demons include Dragons, Balrogs, Nazgul and all act end bosses

Shout duration greatly increased

Impale now uses normal attack (that means it's much faster) it adds 300% damage at level 20 and no tohit 
bonus or other effect

Decoy duration increased and enabled 3 decoys at one time

Valkyrie actually deals reasonable damage (1.10 Blizzard change)

Berserk reduces armorclass to 25% instead of 0%

Cyclone Armor and Bone Armor enhanced

Werebear damage increased

Maul damage increased

Poison Dagger and Poison explosion damage increased

Holy Freeze, Holy Fire, Holy Shock reasonable damage added to attack (around 100-300)

Cleansing and Meditation regenerate health (1/3 of prayer's maximum)

Lightning spells (Sorceress) no longer damage 1-X

"Of the Maiar", "of the Valar" etc. affix series now adds to all four stats

Affixes for jewels and charms overhauled. Many of the existing affixes had unbalanced levels or were 
otherwise badly designed (like having a +1 max damage affix of level 1 and the next in the series was a 
+3 max damage affix of level 89...)

Sauron Reborn's Lightning Bolt of Death can knockback

Bows and crossbows only require dexterity now

Spearclass weapons require less strength

Two handed swords require less dexterity

Items in the gamble list include now all but the most powerful normal and exceptional items (no elite items)

Crossbows are now only slightly slower than Bows

Class specific bows increased in speed

Monsters generally can spawn in smaller or larger groups (group size range increased - were it formerly 3-
5 it is now 2-6 for example)

Exceptional items have a bonus of +2 to their possible magical properties

Elite items have a bonus of +6 to their possible magical properties

Elite Weapons have been added built-in deadly strike, slightly faster cast and slightly faster swing 

Elite Armor has been added built-in physical resistance, slightly faster run and recover stamina

Summoner class specific shields have built-in move faster and regenerate mana instead of added poison 
damage

Double Throw adds 100% damage

Firebolt, Icebolt, magic arrow, fire arrow and ice arrow 50% faster projectile speed

Sorceress skill Enchant replaced with Illusion

Battle Command adds 1 skill level per 7 levels in Battle Command

Inner Sight slows enemy attackspeed

Critical Strike increases linear in %

Blessed Aim adds chance to do double damage

Resist Fire / Lightning / Cold auras add upto 20% max resist in their respective element

New Cube recipes:
(see CubesnRunes.rtf for detailed instructions)

- filled socketed items (non-magic, non-rare, non-set, non-unique!) can be unsocketed keeping the 
socketfillers for later use.

- enhance magic armor +10% AC +3 life but +10% requirements (attributes) - this recipe is repeatable on 
one item

- enhance magic weapon +10% Damage +1 max damage but +10% requirements (attributes) - this recipe 
is repeatable on one item

- create a rare item of 110% quality of the original item from any unique (will be the same itemtype)

- create a magic item of 110% quality of the original item from any rare item (will be the same itemtype)

Changed Cube recipes:
(see CubesnRunes.rtf for details)

- creating a rare with sockets from a rare uses either 100% or 115% or 130% of the original item level as 
new item quality (so player level doesn't count in)

- all runes can now be created from 3 same runes of one level lower quality

- magic items can be morphed into normal items without additional ingredients 

- crafting recipes now use 70% or 80% or 90% of playerlevel to determine quality for the 
corresponding "weak", "normal" and "good" crafting recipes

